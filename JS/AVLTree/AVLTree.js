
class AVLNode{

  constructor(data){
    this.data = data;
    this.left = null;
    this.right = null;
    this.height = 0
  }
}

class AVLTree{


    constructor(){
    this.root = null;
    }

    insert(iData){
    this.root = this.insertNode(this.root, iData);
    }

    delete(iData){
    this.root = this.deleteNode(this.root, iData)
    }

    preOrderTraversal(){
      this.preOrder(this.root ,  "Root");
    }

    insertNode(node, data){

      if (node  == null)  {
        var newNode = new AVLNode(data);
        newNode.height = 1
        return newNode ;
      }

      if(data < node.data) {
        node.left =  this.insertNode(node.left, data);
      } else if(data > node.data){
        node.right =  this.insertNode(node.right,data);
      }else{
         console.log("Duplicate Key");
         return node
      }
      return this.balanceTree(node,data)
    }

    balanceTree(iNode, data){

      var lHt = 0 ;
      var  rHt  = 0

      if (iNode.left != null) {
        lHt = iNode.left.height
      }

      if (iNode.right != null){
        rHt =  iNode.right.height
      }

      iNode.height =   1 + Math.max(lHt, rHt)


      var balance = lHt  -  rHt ;

      if (balance > 1 && data < iNode.left.data) {
          return this.rightRotate(iNode);
      }

      if (balance < -1 && data > iNode.right.data) {
          return this.leftRotate(iNode);
      }

      if (balance > 1 && data > iNode.left.data){
          iNode.left = this.leftRotate(iNode.left);
          return this.rightRotate(iNode);
      }

      if (balance < -1 && data < iNode.right.data) {
        iNode.right =  this.rightRotate( iNode.right);
          return this.leftRotate(iNode);
      }

      return iNode;
    }

    leftRotate( iNode){
        var nodeY = iNode.right;
        var nodeT2 = nodeY.left;
        nodeY.left = iNode;
        iNode.right = nodeT2;

        var lHt = 0 ;
        var  rHt  = 0

        if (iNode.left != null) {
          lHt = iNode.left.height
        }

        if (iNode.right != null){
          rHt =  iNode.right.height
        }

        iNode.height = Math.max(lHt , rHt) + 1;
        nodeY.height = Math.max(nodeY.left.height , nodeY.right.height) + 1;

        return  nodeY ;
      }

    rightRotate(iNode ){

    var nodeX = iNode.left;
    var nodeT2 = nodeX.right;


    nodeX.right = iNode;
    iNode.left = nodeT2;


    var lHt = 0 ;
    var  rHt  = 0

    if (iNode.left != null) {
      lHt = iNode.left.height
    }

    if (iNode.right != null){
      rHt =  iNode.right.height
    }

    iNode.height = Math.max(lHt , rHt) + 1;
    nodeX.height = Math.max(nodeX.left.height , nodeX.right.height) + 1;

    return  nodeX ;
    }

    inorderSuccessorRight(iNode){
      var tempNode = iNode.right
      while (tempNode.left != null){
          tempNode = tempNode.left
      }
      return tempNode
    }

    deleteNode(iNode , iData){
     if (iNode == null){
         return null
     }
     if (iNode.data > iData){
         iNode.left = this.deleteNode(iNode.left, iData)
     }else if (iNode.data < iData){
         iNode.right = this.deleteNode(iNode.right,iData)
     }else{
         var tempNode
         if (iNode.left == null || iNode.right == null) {

             if (iNode.left != null ){
                 tempNode = iNode.left
             }else if (iNode.right != null){
                tempNode = iNode.left
             }else{
                 tempNode = null
                 return tempNode
             }
         }else{
             tempNode = this.inorderSuccessorRight(iNode)
             iNode.data = tempNode.data
             iNode.right = this.deleteNode(iNode.right, tempNode.data)
         }
     }

     if (iNode != null){
        return this.balanceTree(iNode, iData)
     }

     return iNode
    }

    preOrerTraversal(){
        this.preOrder(this.root ,  "Root")
    }

    inOrerTraversal(){
        this.inOrder(this.root ,  "Root")
    }

    postOrderTraversal(){
        this.postOrder(this.root ,  "Root")
    }

    printGivenLevel(iNode, level ,label) {
       if (iNode == null) {
           return
       }
       if (level == 1){
         console.log(label + ": "+ iNode.data);
       }else if (level > 1){
           printGivenLevel(iNode.left , level - 1, "Left Child of : " + iNode.data )
           printGivenLevel(iNode.right , level - 1,"Right Child of : " + iNode.data)
       }
     }

    preOrder(iNode  , side) {
        if(iNode != null) {
            console.log(side +" "+iNode.data);
            this.preOrder(iNode.left, "Left");
            this.preOrder(iNode.right ,"Right");
        }
    }

    inOrder(iNode  , side){

        if(iNode != null) {
            this.inOrder(iNode.left, "Left");
            console.log(side +" "+iNode.data);
            this.inOrder(iNode.right ,"Right");
        }
    }

    postOrder(iNode  , side){

        if(iNode != null) {
            this.postOrder(iNode.left, "Left");
            this.postOrder(iNode.right ,"Right");
            console.log(side +" "+iNode.data);
        }
    }

    traversZigZag(iNode, level ,label,leftToRight){

       if (iNode == null) {
           return
       }
       if (level == 1){
           console.log(label + " : "+ iNode.data);
       }else if (level > 1){
           if (leftToRight == true){
               this.traversZigZag(iNode.left , level - 1, "Left Child of : " + iNode.data,leftToRight)
               this.traversZigZag(iNode.right , level - 1,"Right Child of : " + iNode.data,leftToRight)
           }else{
               this.traversZigZag(iNode.right , level - 1,"Right Child of : " + iNode.data,leftToRight)
               this.traversZigZag(iNode.left , level - 1, "Left Child of : " + iNode.data,leftToRight)
           }
       }
     }

    levelOrderTraversal(){
        var height = this.root.height
        var level
        for (level = 1; level <= height; level++){
            this.printGivenLevel(this.root , level, "Root")
        }
    }

    zigZagOrderTraversal(){
        var height = this.root.height
        var lftToRgt = false
        var level
        for (level = 1; level <= height; level++){
          this.traversZigZag(this.root , level, "Root",lftToRgt)
          lftToRgt = lftToRgt ? false : true
        }
    }
}

var avlTree = new AVLTree()

avlTree.insert(1)
avlTree.insert(2)
avlTree.insert(3)
avlTree.insert(4)
avlTree.insert(5)
avlTree.insert(6)
avlTree.insert(7)
avlTree.preOrderTraversal()
avlTree.delete(4)
avlTree.preOrderTraversal()
avlTree.zigZagOrderTraversal()
