////////////////////////////////////////////////////////
//
//  This software is released as per the clauses of MIT License
//
//
//  The MIT License
//
//  Copyright (c) 2016, Binoy Vijayan.
//
//                      benoy.apple@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//

import Foundation

let avlTree:AvlTree<Int> = AvlTree<Int>()
//avlTree.zigZagOrderTraversal()
//avlTree.levelOrderTraversal()
//avlTree.preOrderTraversal()
//avlTree.inOrderTraversal()
//avlTree.postOrderTraversal()

avlTree.insert(1)
avlTree.insert(2)
avlTree.insert(3)
avlTree.insert(4)
avlTree.insert(5)
avlTree.insert(6)
//avlTree.insert(7)
//avlTree.insert(8)
//avlTree.insert(9)
//avlTree.insert(10)
//avlTree.insert(11)
//avlTree.insert(12)
//avlTree.insert(13)
//avlTree.insert(14)
avlTree.levelOrderTraversal()
avlTree.delete(4)
avlTree.levelOrderTraversal()
avlTree.delete(5)
avlTree.levelOrderTraversal()
avlTree.delete(6)
avlTree.levelOrderTraversal()

//avlTree.inOrderTraversal()
//avlTree.levelOrderTraversal()

//avlTree.delete(12)
//avlTree.delete(11)
//avlTree.delete(10)
//avlTree.delete(9)
//avlTree.delete(1)
//avlTree.delete(3)
//avlTree.zigZagOrderTraversal()
//avlTree.levelOrderTraversal()
//avlTree.preOrderTraversal()
//avlTree.inOrderTraversal()
//avlTree.postOrderTraversal()
print("\n\n")

//avlTree.delete(4)
//
//avlTree.zigZagOrderTraversal()
//avlTree.levelOrderTraversal()
//avlTree.preOrderTraversal()
//avlTree.inOrderTraversal()
//avlTree.postOrderTraversal()







