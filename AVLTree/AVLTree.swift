////////////////////////////////////////////////////////
//
//  This software is released as per the clauses of MIT License
//
//
//  The MIT License
//
//  Copyright (c) 2016, Binoy Vijayan.
//
//                      benoy.apple@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//

import Foundation
class AvlTreeNode<T:Comparable>{

    var data:T? = nil
    var left: AvlTreeNode? = nil
    var right: AvlTreeNode? = nil
    var height:Int = 0
    init( _ iData:T){
        self.data = iData
    }
}

class AvlTree<T:Comparable>{
    
    typealias AvlNode = AvlTreeNode<T>
    
    var rootNode:AvlNode? = nil
    
    func insert(_ iData:T){
       rootNode =  insertNode(rootNode , iData )
    }
    
    func delete(_ iData:T){
       rootNode = deleteNode(rootNode, iData)
    }
    
    func preOrderTraversal(){
        preOrder(rootNode ,  "Root")
    }
    
    func postOrderTraversal(){
        postOrder(rootNode ,  "Root")
    }
    
    func inOrderTraversal(){
        inOrder(rootNode ,  "Root")
    }
    
    func levelOrderTraversal(){
        guard let height = rootNode?.height else{
            return
        }
        for level in 1...height{
            printGivenLevel(rootNode , level, "Root")
        }
    }
    
    func zigZagOrderTraversal(){
        
        guard let height = rootNode?.height else{
            return
        }
        
        var lftToRgt = false
        for level in 1...height{
            traversZigZag(rootNode , level, "Root",lftToRgt)
            lftToRgt = !lftToRgt
        }
    }

    /* Private Functions*/
    
    private func traversZigZag(_ iNode:AvlNode?, _ level:Int , _ label:String, _ leftToRight:Bool ){
        
        if iNode == nil {
            return
        }
        if level == 1{
            print("\(label) :\(iNode!.data!)")
        }else if level > 1{
            
            if leftToRight == true{
                traversZigZag(iNode?.left , level - 1, "Left Child of \(iNode!.data!)",leftToRight)
                traversZigZag(iNode?.right , level - 1,"Right Child of \(iNode!.data!)",leftToRight)
            }else{
                traversZigZag(iNode?.right , level - 1,"Right Child of \(iNode!.data!)",leftToRight)
                traversZigZag(iNode?.left , level - 1, "Left Child of \(iNode!.data!)",leftToRight)
            }
        }
    }
    
    private func printGivenLevel(_ iNode:AvlNode?, _ level:Int , _ label:String) {
        if iNode == nil {
            return
        }
        if level == 1{
            print("\(label) :\(iNode!.data!)")
        }else if level > 1{
            printGivenLevel(iNode?.left , level - 1, "Left Child of \(iNode!.data!)")
            printGivenLevel(iNode?.right , level - 1,"Right Child of \(iNode!.data!)")
        }
    }
    
    private func preOrder(_ iNode:AvlNode?  , _ side:String) {
        
        guard let data = iNode?.data  else{
            return
        }
        print("\(side) : \(data) \n")
        preOrder(iNode?.left, "Left")
        preOrder(iNode?.right ,"Right")
        
    }
    
    private func postOrder(_ iNode:AvlNode?  , _ side:String){
        
        guard let data = iNode?.data  else{
            return
        }
        postOrder(iNode?.left, "Left")
        postOrder(iNode?.right ,"Right")
        print("\(side) : \(data) \n")
        
    }
    
    private func inOrder(_ iNode:AvlNode?  , _ side:String){
        
        guard let data = iNode?.data  else{
            return
        }
        postOrder(iNode?.left, "Left")
        print("\(side) : \(data) \n")
        postOrder(iNode?.right ,"Right")
    }
    
    private func insertNode(_ iNode:AvlNode? , _ iData:T) -> AvlNode?{
        
        if iNode == nil{
            let nd = AvlNode(iData)
            nd.height = 1
            return nd
        }
        
        if iData < iNode!.data! {
            iNode!.left = insertNode(iNode!.left , iData)
        }else if iData > iNode!.data!{
            iNode!.right = insertNode(iNode!.right , iData)
        }else{
            return iNode
        }
        
        return balanceTree(iNode , iData)
    }
    
    private func balanceTree( _ iNode:AvlNode? , _ iData:T)->AvlNode?{
        
        if iNode == nil {
            return nil
        }
        
        iNode!.height = 1 + max(iNode!.left?.height ?? 0, iNode!.right?.height ?? 0)
        let balance = (iNode!.left?.height ?? 0)  -  (iNode!.right?.height ?? 0)
        
        if  let data =  iNode?.left?.data {
            if balance > 1  {
                return rightRotate(iNode!)
            }
            if balance > 1 && iData > data{
                iNode!.left =  leftRotate(iNode!.left!)
                return rightRotate(iNode!)
            }
        }
        
        if let data = iNode?.right?.data {
            if balance < -1  {
                return leftRotate(iNode!)
            }
            
            if balance < -1 && iData < data{
                iNode!.right = rightRotate( iNode!.right!)
                return leftRotate(iNode!)
            }
        }
        
        return iNode
    }
    
    private func inorderSuccessorRight( _ iNode:AvlNode?)-> AvlNode?{
        var tempNode:AvlNode? = iNode
        while  tempNode?.left != nil {
            tempNode = tempNode?.left
        }

        return tempNode
    }
    
    private func getMinValueForNode(_ iNode:AvlNode?) -> T?{
        if iNode == nil  {
            return  nil
        }
        if iNode!.left == nil{
          return iNode!.data
        }
        return getMinValueForNode(iNode!.left);
    }
    
    private func deleteNode(_ iNode:AvlNode? , _ iData:T)->AvlNode? {
        
        if iNode == nil {
           return iNode
        }
        if iData < iNode!.data! {
            iNode!.left = deleteNode(iNode!.left, iData)
        }else if iData > iNode!.data! {
            iNode!.right = deleteNode(iNode!.right, iData)
        }else {
            if iNode!.left == nil {
                return iNode!.right
            }else if iNode!.right == nil {
                return iNode!.left
            }else{
                iNode!.data = getMinValueForNode(iNode!.right)
                iNode!.right = deleteNode(iNode!.right, iNode!.data!)
            }
        }

        return self.balanceTree(iNode, iNode!.data!)
    }

    private func leftRotate( _ iNode:AvlNode) -> AvlNode?{
        
        let nodeY = iNode.right
        let nodeT2 = nodeY?.left

        nodeY?.left = iNode
        iNode.right = nodeT2
        
        iNode.height = max(iNode.left?.height ?? 0 , iNode.right?.height ?? 0) + 1
        nodeY?.height = max(nodeY?.left?.height ?? 0 , nodeY?.right?.height ?? 0) + 1
        
        return nodeY
    }
    
    private func rightRotate(_ iNode:AvlNode ) -> AvlNode?{
        
        let nodeX = iNode.left
        let nodeT2 = nodeX?.right
        
        nodeX?.right = iNode
        iNode.left = nodeT2

        iNode.height = max(iNode.left?.height ?? 0 , iNode.right?.height ?? 0) + 1
        nodeX?.height = max(nodeX?.left?.height ?? 0 , nodeX?.right?.height ?? 0) + 1
        
        return nodeX
    }
}

