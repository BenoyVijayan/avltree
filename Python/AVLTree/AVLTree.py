////////////////////////////////////////////////////////
//
//  This software is released as per the clauses of MIT License
//
//
//  The MIT License
//
//  Copyright (c) 2016, Binoy Vijayan.
//
//                      benoy.apple@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//

class AVLNode:

    left = None
    right = None
    height = 0

    def __init__(self, data):
        self.data = data

class AVLTree :

    def __init__(self):
        self.root = None

    def insert(self, data):
        self.root = self.insertNode(self.root,data)

    def delete(self, data) :
        self.root = self.deleteNode(self.root, data)

    def preOrderTraversal(self) :
        self.preOrder(self.root ,  "Root")

    def inOrderTraversal(self) :
        self.inOrder(self.root ,  "Root")

    def postOrderTraversal(self) :
        self.postOrder(self.root ,  "Root")

    def levelOrderTraversal(self) :
        height = self.root.height
        for level in range(1,height + 1):
            self.printGivenLevel(self.root , level, "Root")

    def zigZagOrderTraversal(self) :
        height = self.root.height
        lftToRgt = False
        for level in range(1,height + 1) :
            self.traversZigZag(self.root , level, "Root",lftToRgt)
            lftToRgt = False if lftToRgt else True

# Private methods   ***********************************************************
    def insertNode(self, node , data):
        if node == None :
            node = AVLNode(data)
            print("Data {}".format(data))
            node.height = 1
            return node

        if data < node.data :
            node.left = self.insertNode(node.left, data)
        elif data > node.data :
            node.right = self.insertNode(node.right, data)
        else :
            return node

        return self.balanceTree(node ,data)

    def balanceTree(self, node , data):

        lht = 0
        rht = 0
        if node.left != None :
            lht = node.left.height
        if node.right != None :
            rht = node.right.height

        node.height = 1 + max(lht , rht)
        balance = lht - rht

        if balance > 1 and data < node.left.data :
            return self.rightRotate(node)

        if balance < -1 and data > node.right.data :
            return self.leftRotate(node)

        if balance > 1 and data > node.left.data :
            node.left =  self.leftRotate(node.left)
            return self.rightRotate(node)

        if balance < -1 and data < node.right.data :
            node.right = self.rightRotate( node.right)
            return self.leftRotate(node)

        return node


    def rightRotate(self, node) :

        nodeY = node.left
        nodeT2 = node.right
        nodeY.left = nodeT2
        node.right = node

        lht = 0
        rht = 0
        if node.left != None :
            lht = node.left.height
        if node.right != None :
            rht = node.right.height

        node.height = 1 + max(lht , rht)

        if nodeY.left != None :
            lht = nodeY.left.height
        if nodeY.right != None :
            rht = nodeY.right.height

        nodeY.height = 1 + max(lht , rht)

        return nodeY

    def leftRotate(self, node) :

        nodeY = node.right
        nodeT2 = nodeY.left

        nodeY.left = node
        node.right = nodeT2

        lht = 0
        rht = 0
        if node.left != None :
            lht = node.left.height
        if node.right != None :
            rht = node.right.height
        node.height = 1 + max(lht , rht)

        if nodeY.left != None :
            lht = nodeY.left.height
        if nodeY.right != None :
            rht = nodeY.right.height

        nodeY.height = 1 + max(lht , rht)
        return nodeY

    def preOrder(self,node,side) :
         if node != None :
             print("{} : {}".format(side, node.data))
             self.preOrder(node.left, "Left")
             self.preOrder(node.right ,"Right")

    def postOrder(self, node , side) :
         if node != None :
             self.preOrder(node.left, "Left")
             self.preOrder(node.right ,"Right")
             print("{} : {}".format(side, node.data))

    def inOrder(self, node , side) :
         if node != None :
             self.preOrder(node.left, "Left")
             print("{} : {}".format(side, node.data))
             self.preOrder(node.right ,"Right")

    def traversZigZag(self, node, level , label, leftToRight ) :
        if node == None :
            return
        if level == 1 :
            print("{} : {}".format(label,node.data))
        elif level > 1 :
            if leftToRight == True :
                self.traversZigZag(node.left , level - 1, "Left Child of : {}".format(node.data),leftToRight)
                self.traversZigZag(node.right , level - 1,"Right Child of : {}".format(node.data),leftToRight)
            else :
                self.traversZigZag(node.right , level - 1,"Right Child of : {}".format(node.data),leftToRight)
                self.traversZigZag(node.left , level - 1, "Left Child of : {}".format(node.data),leftToRight)

    def printGivenLevel(self,node,level ,label) :
        if node == None :
            return
        if level == 1 :
            print("{} : {}".format(label, node.data))
        elif level > 1 :
            self.printGivenLevel(node.left , level - 1, "Left Child of :{}".format(node.data))
            self.printGivenLevel(node.right , level - 1,"Right Child of :{}".format(node.data))

    def inorderSuccessorRight( self, node) :
        tempNode = node.right
        while tempNode.left != None :
            tempNode = tempNode.left
        return tempNode

    def deleteNode(self, node , data) :
        if node == None :
            return None
        if node.data > data :
            node.left = self.deleteNode(node.left,data)
        elif node.data < data :
            node.right = self.deleteNode(node.right,data)
        else :
            if node.left == None or node.right == None :

                if node.left != None :
                    tempNode = node.left
                elif node.right != None :
                   tempNode = node.left
                else :
                    tempNode = None
                    return tempNode
            else :
                tempNode = self.inorderSuccessorRight(node)
                node.data = tempNode.data
                node.right = self.deleteNode(node.right, tempNode.data)

        if node != None :
           return self.balanceTree(node, data)

        return node


tree = AVLTree()
tree.insert(1)
tree.insert(2)
tree.insert(3)
tree.insert(4)
tree.insert(5)
tree.insert(6)
tree.insert(7)
tree.insert(8)
tree.insert(9)
tree.insert(10)
tree.insert(11)
tree.insert(12)
tree.insert(13)

tree.delete(9)
tree.delete(8)

tree.levelOrderTraversal()
